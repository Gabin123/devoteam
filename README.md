# Devoteam

Test technique réalisé dans le cadre d'une candidature.
Design inspiré de : https://www.behance.net/gallery/52461471/Minim-E-commerce-Website?tracking_source=search%257Clanding%2Bpage

Le front est réalisé avec angular et le back en nodejs avec le framework NestJs.

# Versions utilisées

Angular CLI: 8.3.8
Node: 12.11.1
Angular: 8.2.9
Nest 6.10.1

# Installation des cli

Angular
npm install -g @angular/cli

NestJs
npm i -g @nestjs/cli

# Lancement

Réaliser un "npm install" sur le front et le back avant le lancement.

Front (./minim-front)
npm run start

Back (./minim-back)
npm run start 
