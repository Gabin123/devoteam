import { Injectable } from '@nestjs/common';

import { ProductInterface } from '../interfaces/product.interface';

@Injectable()
export class ProductService {

    private products: ProductInterface[];

    constructor() {}

    getProducts(): ProductInterface[] {

        this.products = [];
        
        for (var i = 0; i < 8; i++) {
            this.products.push({
                id: i,
                name: this.getRandomName(9),
                author: this.getRandomName(5) + ' ' + this.getRandomName(7),
                price: this.getRandomInt(160)
            });
        }
        return this.products;
    }

    getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    getRandomName(length) {
        var result           = '';
        var upperConsonant       = 'BCDFGHJKLMNPQRSTVWXYZ';
        var lowerConsonant       = 'bcdfghjklmnpqrstvwxyz';
        var lowerVowel= 'aeiou';

        result += upperConsonant.charAt(this.getRandomInt(21));

        for ( var i = 0; i < (length - 1) / 2; i++ ) {
           result += lowerVowel.charAt(this.getRandomInt(5));
           result += lowerConsonant.charAt(this.getRandomInt(21));
        }
        return result;
     }
        
}
