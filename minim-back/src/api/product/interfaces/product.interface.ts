export interface ProductInterface {
	id: number;
	name: string;
	author: string;
	price: number;
}