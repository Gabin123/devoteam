export class Product {
	id: string;
	name: string;
	author: string;
	price: number;
}