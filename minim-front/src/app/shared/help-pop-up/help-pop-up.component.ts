import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-help-pop-up',
  templateUrl: './help-pop-up.component.html',
  styleUrls: ['./help-pop-up.component.scss']
})
export class HelpPopUpComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
