import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ProductComponent } from './product/product.component';
import { HelpPopUpComponent } from './help-pop-up/help-pop-up.component';

@NgModule({
  declarations: [
    ProductComponent,
    HelpPopUpComponent
  ],
  imports: [
    NgbModule
  ],
  entryComponents:[
    HelpPopUpComponent
  ],
  exports: [
    ProductComponent,
    HelpPopUpComponent
  ]
})
export class SharedModule { }
