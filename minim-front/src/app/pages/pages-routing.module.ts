import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { ProductListComponent } from './product-list/product-list.component';
import { HomeComponent } from './home/home.component';
import { ProductListModule } from './product-list/product-list.module';
import { HomeModule } from './home/home.module';

const routes: Routes = [
      {
        path: 'product-list',
        component: ProductListComponent
      },
      {
        path: '',
        component: HomeComponent
      },
      { path: '**', component: HomeComponent }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ProductListModule,
    HomeModule
  ],
  exports: [
    RouterModule
  ]
})
export class PagesRoutingModule { }