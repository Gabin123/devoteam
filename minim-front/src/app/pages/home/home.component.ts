import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Product } from '../../../app/shared/models/product.model';
import { ProductService } from '../../../app/core/product/product.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HelpPopUpComponent } from 'src/app/shared/help-pop-up/help-pop-up.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public products$: Observable<Product[]>;

  constructor(public router: Router,
              public productService: ProductService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(): void {
    this.products$ = this.productService.getProducts();
  }

  openHelpPopUp() {
    this.modalService.open(HelpPopUpComponent, {size: "xl"});
  }
}
